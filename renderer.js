const remote = require("electron").remote;
const mainWindow = remote.getCurrentWindow();
$("#searchButton").click(function(){
    let adres = $("#mainSearch input").val();
    if (!adres.startsWith("http://www.tvseriesonline.pl/"))
    {
        remote.dialog.showErrorBox("Problem z wyszukiwaniem", "Podany adres jest niepoprawny");
        return;
    }
    let promise = TSApi.LoadEpisodesUrls(adres);
    let __loaderAnimationEnded = false;
    $("#mainSearch").fadeOut(() => {
        $("#loader").fadeIn(() => { __loaderAnimationEnded = true});
    });


    promise.then((episodes) => {
        //episodes = array({Title: ..., URL: ...});
        let epPromises = [];
        for (let episode of episodes)
            epPromises.push(TSApi.LoadEpisodeHosting(episode));
        return Promise.all(epPromises);
    })
    .then((episodes) => {
        let epPromises = [];
        for (let episode of episodes)
            epPromises.push(TSApi.LoadEpisodeThumb(episode));
        return Promise.all(epPromises);
    })
    .then((episodes) => {
        //episodes = [{Title, URL, Hosting, Image}]
        $("#episodesList").children().remove();
        let i = 0;
        for (let ep of episodes)
        {
            $("<div />", {
                style: "background-image: url(" + ep.Image + ")",
                class: i == 0 ? "active" : "",
                url: ep.Hosting
            }).text(ep.Title).appendTo($("#episodesList"));
            if (i == 0)
                $("#video webview").attr("src", ep.Hosting.replace("/f/", "/embed/"));
            i++;
        }
        $("#episodesList").children().click(function() {
           if ($(this).hasClass("active"))
               return;
            $("#episodesList .active").removeClass("active");
            $(this).addClass("active");
            $("#video webview").attr("src", $(this).attr("url").replace("/f/", "/embed/"));
        });
        $("#loader").fadeOut(() => {
           $("#sectionEpisodes").css("display", "flex");
        });
    })
    .catch((err) => {
        function _backToMain() {
            if (!__loaderAnimationEnded)
                return;
            clearInterval(_btmInterval);
            remote.dialog.showErrorBox("Problem z wyszukiwaniem", "Podany serial posiada nieobsługiwane odcinki");
            $("#loader").fadeOut(() => {$("#mainSearch").fadeIn() });
        };        
        let _btmInterval = setInterval(_backToMain, 115);
    });

});

//film
(() => {
    let webview =  document.querySelector("#video webview");
    webview.addEventListener("enter-html-full-screen", function() {
        $("#episodesList").hide();
    });
    webview.addEventListener("leave-html-full-screen", function() {
        $("#episodesList").show();
    });
})();


class TSApi {
    static LoadEpisodesUrls(url) {
        return new Promise((resolve, reject) => {
            $.get(url, (res) => {
                let m;
                let regex = /h4 class="title"><a href="(.*?)"[^]*?title="(.*?)"/gmi;

                let episodes = [];
                while ((m = regex.exec(res)) !== null)
                {
                    if (m.index === regex.lastIndex)
                        regex.lastIndex++;

                    episodes.push({
                        Title: $("<div/>").html(m[2]).text(), //html
                        URL: m[1]
                    });
                }
                resolve(episodes.reverse());
            });
        });
    }
    static LoadEpisodeHosting(episode) {
        return new Promise((resolve, reject) => {
            $.get(episode.URL, (res) => {
                let match = res.match(/http[s]?:\/\/openload.*?\/(.*?)"/);
                if (match === null) {
                    console.log("REJECT:");
                    console.log(episode);
                    reject();
                }
                else {
                    episode.Hosting = "https://openload.co/" + match[1];
                    resolve(episode);
                }
            });
        });
    }
    static LoadEpisodeThumb(episode) {
        return new Promise((resolve, reject) => {
            $.get(episode.Hosting, (res) => {
                let match = res.match(/og:image" content="(.*?)"/);
                if (match === null)
                    episode.Image = "/img/404.png";
                else
                    episode.Image = match[1];
                resolve(episode);
            });
        });
    }
}